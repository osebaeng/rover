# README #
Running the program:

Compile RoverTest.java, then execute passing in file name with commands for rover. 
File contents have to be formatted exactly same way as specified in requirements brief.

Design:

Solution inspired from Object Oriented Design approach, as OO based design are much easier to extend and mainatain. 

The Rover move function with nested Switch Case statements can be improved. 

Added additional check function to validate if proposed move for a rover is valid, i.e a move in that direction is not outside 
the bounds of the current zone being surveyed. Unit test also reqiured for each function. 




	